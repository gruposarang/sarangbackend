name := """SarangBackend"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  "org.mindrot" % "jbcrypt" % "0.3m",
  "mysql" % "mysql-connector-java" % "5.1.35",
  "com.typesafe.play" % "play-mailer_2.11" % "5.0.0-M1",
  "net.coobird" % "thumbnailator" % "0.4.8",
  evolutions,
  javaJdbc,
  cache,
  javaWs
)
