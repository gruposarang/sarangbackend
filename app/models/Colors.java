package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */

@Entity
public class Colors extends Model {

    @Id
    public Long id;

    @NotNull
    @Column(unique = true)
    public String name;

    @NotNull
    public String hex;

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Colors> find(){
        return new Finder<>(Colors.class);
    }

    public static List<Colors> findAll(){
        return find().all();
    }

    public static Colors findById(Long id){
        return find().byId(id);
    }
    
    public static Colors create(String name, String hex) {
        Colors checkColor = find().where().eq("name", name).findUnique();
        if (checkColor == null){
            Colors color = new Colors();
            color.name = name;
            color.hex = hex;
            color.save();
            return color;
        }
        return null;
        
    }
    
    public static Colors update(Colors updatedColor){
        updatedColor.update();
        return updatedColor;
    }
}
