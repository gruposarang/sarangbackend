package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class CartsProducts extends Model {

    @Id
    public Long id;

    @NotNull
    @ManyToOne
    public Products product;

    @NotNull
    @ManyToOne
    @JsonBackReference
    public Carts cart;

    @NotNull
    public int quantity = 0;


    private static Finder<Long, CartsProducts> find(){
        return new Finder<>(CartsProducts.class);
    }

    public static List<CartsProducts> findAll(){
        return find().all();
    }

    public static CartsProducts findById(long id){
        return find().byId(id);
    }

    public static CartsProducts create(int quantity, Products product, Carts cart){
        CartsProducts cartsProducts = new CartsProducts();
        cartsProducts.product = product;
        cartsProducts.cart = cart;
        cartsProducts.quantity = quantity;
        cartsProducts.save();
        return cartsProducts;
    }

}
