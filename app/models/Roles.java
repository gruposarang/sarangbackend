package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Roles extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<Users> users = new ArrayList<>();

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Roles> find(){
        return new Finder<>(Roles.class);
    }

    public static List<Roles> findAll(){
        return find().all();
    }

    public static Roles findById(long id){
        return find().byId(id);
    }

    public static Roles create(String name){
        Roles existentRole = find().where().eq("name", name).findUnique();
        if (existentRole == null){
            Roles newRole = new Roles();
            newRole.name = name;
            newRole.save();
            return newRole;
        }
        return null;
    }

    public static Roles update(Roles updatedRole){
        updatedRole.update();
        return updatedRole;
    }
}
