package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import play.mvc.Http;
import play.Logger;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Orders extends Model {

    @Id
    public Long id;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    public List<HistoricalPrices> historicalPrices = new ArrayList<>();

    @NotNull
    public boolean isPickUp = true;

    @ManyToOne
    public Addresses address;

    @NotNull
    public Statuses status = Statuses.Abierta;

    @Column(columnDefinition = "TEXT")
    public String note;

    @UpdatedTimestamp
    public Date updatedAt;

    @NotNull
    @ManyToOne
    public Users user;

    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Orders> find(){
        return new Finder<>(Orders.class);
    }

    public static List<Orders> findAll(){
        return find().orderBy("id desc").findList();
    }

    public static Orders findById(long id){
        return find().byId(id);
    }
    
    public static List<Orders> findByUser(long userId){
        return find().where().eq("user", Users.findById(userId)).orderBy("id desc").findList();
    }

    public static int count(){
        return find().findRowCount();
    }

    public static List<Orders> between(int pagedSize, int pagedIndex){
        return find().orderBy("id desc").findPagedList(pagedIndex, pagedSize).getList();
    }

    public static Orders changeStatus(Long orderId, Statuses status){
        Orders order = findById(orderId);
        order.status = status;
        order.update();
        return order;
    }

    public static Orders create(String note){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        if (token != null) {
            AccessTokens accessTokens = AccessTokens.findOne(token);
            Carts cart = Carts.findByUser(accessTokens.user.id);

            if (cart.cartsProducts.size() > 0) {

                Orders order = new Orders();
                order.user = accessTokens.user;
                Logger.info("USER EMAIL" + order.user.email);
                order.note = note;
                order.save();


                for (CartsProducts cartsProducts : cart.cartsProducts) {
                    HistoricalPrices historicalPrices = new HistoricalPrices();
                    historicalPrices.order = order;
                    historicalPrices.quantity = cartsProducts.quantity;
                    historicalPrices.product = cartsProducts.product;
                    historicalPrices.price = cartsProducts.product.price;
                    historicalPrices.save();
                }
                cart.cartsProducts = new ArrayList<>();
                cart.update();
                return order;
            }
        }
        return null;
    }

}
