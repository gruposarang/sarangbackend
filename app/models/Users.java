package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.JsonIgnore;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.mindrot.jbcrypt.BCrypt;
import play.data.validation.Constraints;
import play.libs.Json;
import play.mvc.Http;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Ariel Guzman on 5/31/2017
 */

@Entity
public class Users extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @NotNull
    public String lastName;

    @NotNull
    @Column(unique = true)
    @Constraints.Email
    public String email;

    @NotNull
    @JsonIgnore
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    public String password;

    @NotNull
    public boolean isMailVerified = false;


    public Statuses status = Statuses.Inactive;

    @ManyToOne
    public Roles role;

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;


    private static Finder<Long, Users> find(){
        return new Finder<>(Users.class);
    }

    public static List<Users> findAll(){
        return find().all();
    }

    public static Users findById(long id){
        return find().byId(id);
    }

    public static Users create(String name, String lastName, String email, String password, Long roleId){
        Users existentUser = find().where().eq("email", email).findUnique();
        if (existentUser == null){
            Users newUser = new Users();
            newUser.name = name;
            newUser.lastName = lastName;
            newUser.email = email;
            newUser.role = Roles.findById(roleId);
            newUser.password = BCrypt.hashpw(password, BCrypt.gensalt());
            newUser.save();
            Carts.create(newUser);
            WishLists.create(newUser);
            return newUser;
        }
        return null;
    }

    public static Users update(Users updatedUser){
        updatedUser.update();
        return updatedUser;
    }

    public static int logout(){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        if (token != null) {
            AccessTokens accessTokens = AccessTokens.findOne(token);
            if (accessTokens != null) {
                accessTokens.delete();
                return 0;
            } else {
                return 1;
            }
        } else {
            return 2;
        }
    }

    public static AccessTokens login(String email, String password){
        Users user = find().where().eq("email", email).findUnique();
        if(user != null && BCrypt.checkpw(password, user.password)){
            AccessTokens accessToken = new AccessTokens();
            accessToken.user = user;
            accessToken.token = UUID.randomUUID().toString();
            accessToken.save();
            return accessToken;
        } else {
            return null;
        }
    }

    public static JsonNode passwordRecover(String email){
        Users existentUser = find().where().eq("email", email).findUnique();

        if (existentUser != null){
            SecureRandom random = new SecureRandom();
            String newPassword = new BigInteger(130, random).toString(32);
            existentUser.password = BCrypt.hashpw(newPassword, BCrypt.gensalt());
            existentUser.save();
            ObjectNode result = Json.newObject();
            result.put("name", existentUser.name);
            result.put("password", newPassword);

            return result;
        }
        return null;
    }
}
