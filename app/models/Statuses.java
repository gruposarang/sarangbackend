package models;

/**
 * Created by Ariel Guzman on 7/17/2017
 */
public enum Statuses {

    //General Statuses
    Inactive,
    Active,

    //Order Statuses
    Abierta,
    Pagada,
    Entregada

}
