package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class HistoricalPrices extends Model {

    @Id
    public Long id;

    @NotNull
    @ManyToOne
    public Products product;

    @NotNull
    @ManyToOne
    @JsonBackReference
    public Orders order;

    @NotNull
    public float price = 0;

    @NotNull
    public int quantity = 0;

    private static Finder<Long, HistoricalPrices> find(){
        return new Finder<>(HistoricalPrices.class);
    }

    public static List<HistoricalPrices> findAll(){
        return find().all();
    }

    public static HistoricalPrices findById(long id){
        return find().byId(id);
    }

}
