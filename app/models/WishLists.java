package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.JsonIgnore;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.mvc.Http;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */

@Entity
public class WishLists extends Model {

    @Id
    public Long id;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<Products> products = new ArrayList<>();

    @NotNull
    @OneToOne
    @JsonIgnore
    public Users user;

    @UpdatedTimestamp
    public Date updatedAt;

    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, WishLists> find(){
        return new Finder<>(WishLists.class);
    }

    public static List<WishLists> findAll(){
        return find().all();
    }

    public static WishLists findById(Long id){
        return find().byId(id);
    }

    public static WishLists findByUser(Long id){
        return find().where().eq("user", Users.findById(id)).findUnique();
    }

    public static WishLists create(Users user){
        WishLists wishList = new WishLists();
        wishList.user = user;
        wishList.save();
        return wishList;
    }

    public static WishLists addToWishList(Long productId){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        AccessTokens accessTokens = AccessTokens.findOne(token);
        WishLists wishList = findByUser(accessTokens.user.id);
        if (wishList != null){
            Products product = Products.findById(productId);
            wishList.products.add(product);
            wishList.update();
            return wishList;
        }
        return null;

    }
}
