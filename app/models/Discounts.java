package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.databind.JsonNode;
import play.Logger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Discounts extends Model {

    @Id
    public Long id;

    @NotNull
    @OneToOne
    @JsonBackReference
    public Products product;

    public Float percentage;

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Discounts> find(){
        return new Finder<>(Discounts.class);
    }

    public static List<Discounts> findAll(){
        return find().all();
    }

    public static Discounts findById(long id){
        return find().byId(id);
    }

    public static Discounts addDiscount(JsonNode requestProduct, float percentage){
        Products product = Products.findById(requestProduct.get("id").asLong());
        if (product != null){
//            Logger.info();
           Discounts discount = find().where().eq("product", product).findUnique();
           if (discount != null){
               discount.percentage = percentage;
               discount.update();
           } else {
               discount = new Discounts();
               discount.percentage = percentage;
               discount.product = product;
               discount.save();
           }
            return discount;
        }
        Logger.error("Did not find a product for the discount");
        return null;

    }

}
