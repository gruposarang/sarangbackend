package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
@Entity
public class AccessTokens extends Model {
    @Id
    public Long id;

    @NotNull
    public String token;

    @NotNull
    @ManyToOne
    @JsonManagedReference
    public Users user;

    @NotNull
    public int ttl = 1209600;

    @CreatedTimestamp
    public Date createdAt;

    @UpdatedTimestamp
    public Date updatedAt;

    public static Finder<Long, AccessTokens> find() {
        return new Model.Finder<>(AccessTokens.class);
    }

    public static AccessTokens findOne(String token){
        return find().where().eq("token", token).findUnique();
    }
}
