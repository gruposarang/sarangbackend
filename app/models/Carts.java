package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.JsonIgnore;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import play.mvc.Http;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Carts extends Model {

    @Id
    public Long id;

    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<CartsProducts> cartsProducts = new ArrayList<>();

    @NotNull
    @OneToOne
    @JsonIgnore
    public Users user;


    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;


    private static Finder<Long, Carts> find(){
        return new Finder<>(Carts.class);
    }


    public static List<Carts> findAll(){
        return find().all();
    }

    public static Carts findById(Long id){
        return find().byId(id);
    }

    public static Carts findByUser(Long id){
        return find().where().eq("user", Users.findById(id)).findUnique();
    }

    public static Carts create(Users user){
        Carts cart = new Carts();
        cart.user = user;
        cart.save();
        return cart;
    }

    public static Carts addToCart(int quantity, Long productId){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        AccessTokens accessTokens = AccessTokens.findOne(token);
        if (accessTokens != null){
            Carts cart = findByUser(accessTokens.user.id);
            if (cart != null){
                Products product = Products.findById(productId);
                CartsProducts cartsProducts = CartsProducts.create(quantity, product, cart);
                cart.cartsProducts.add(cartsProducts);
                cart.save();
                return cart;
            }
        }

        return null;

    }
}
