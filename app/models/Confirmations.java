package models;

import com.avaje.ebean.Model;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by Ariel Guzman on 6/20/2017
 */
@Entity
public class Confirmations extends Model {

    @Id
    public long id;

    @NotNull
    public String confirmToken;

    @NotNull
    @ManyToOne
    @JsonManagedReference
    public Users user;

    private static Finder<Long, Confirmations> find(){
        return new Finder<>(Confirmations.class);
    }

    public static Confirmations create(Users user){
        Confirmations confirmation = new Confirmations();
        confirmation.user = user;
        confirmation.confirmToken = UUID.randomUUID().toString();
        confirmation.save();
        return confirmation;
    }

    public static Confirmations byToken(String confirmToken){
        return find().where().eq("confirmToken", confirmToken).findUnique();
    }

    public static Confirmations byUser(Users users){
        return find().where().eq("users", users).findUnique();
    }

}