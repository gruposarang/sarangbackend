package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.JsonNode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Products extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @NotNull
    public Float price;

    @Column(columnDefinition = "TEXT")
    public String description;

    public int quantity;

    public Statuses status = Statuses.Active;

    public String photo;
    public String photoThumbnail;

    @OneToOne(mappedBy = "product")
    public Discounts discount;

    @NotNull
    @ManyToOne
    public Categories category;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<Colors> colors = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonManagedReference
    public List<Sizes> sizes = new ArrayList<>();

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Products> find() {
        return new Finder<>(Products.class);
    }

    public static List<Products> findAll(){
        return find().orderBy("id desc").findList();
    }

    public static Products findById(Long id){
        return find().byId(id);
    }

    public static int count(){
        return find().findRowCount();
    }

    public static List<Products> between(int pagedSize, int pagedIndex){
        return find().orderBy("id desc").findPagedList(pagedIndex, pagedSize).getList();
    }

    public static Products create(String name, Float price, String description, int quantity,
                                  Long categoryId, JsonNode colors, JsonNode sizes){
        Products existingProduct = find().where().eq("name", name).findUnique();

        if (existingProduct == null){
            Products newProduct = new Products();
            newProduct.name = name;
            newProduct.price = price;
            newProduct.description = description;
            newProduct.quantity = quantity;
            newProduct.category = Categories.findById(categoryId);

            for (int i = 0; i < colors.size(); i++) {
                Colors color = Colors.findById(colors.get(i).findPath("id").asLong());
                newProduct.colors.add(color);
            }

            for (int i = 0; i < sizes.size(); i++) {
                Sizes size = Sizes.findById(sizes.get(i).findPath("id").asLong());
                newProduct.sizes.add(size);
            }
            newProduct.save();
            return newProduct;
        }
        return null;
    }

    public static Products update(Products updatedProduct){
        updatedProduct.update();
        return updatedProduct;
    }

    public static Products addPhoto(long productId, String path, String thumbnail){
        Products product = findById(productId);
        product.photo = path;
        product.photoThumbnail = thumbnail;
        product.update();
        return product;
    }
}
