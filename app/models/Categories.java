package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Categories extends Model {

    @Id
    public Long id;

    @NotNull
    public String name;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    @JsonBackReference
    public List<Products> products = new ArrayList<>();

    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Categories> find(){
        return new Finder<>(Categories.class);
    }

    public static List<Categories> findAll(){
        return find().all();
    }

    public static Categories findById(Long id){
        return find().byId(id);
    }

    public static Categories update(Categories updatedCategory){
        updatedCategory.update();
        return updatedCategory;
    }

    public static Categories create(String name){
        Categories checkCategories = find().where().eq("name", name).findUnique();
        if (checkCategories == null){
            checkCategories = new Categories();
            checkCategories.name = name;
            checkCategories.save();
            return checkCategories;
        }
        return null;
    }
}
