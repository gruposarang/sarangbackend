package models;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.Logger;
import play.libs.Json;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Result;
import play.mvc.Results;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class SecuredAction extends Action.Simple {
    @Override
    public CompletionStage<Result> call(Http.Context context)  {
        String token = getTokenFromHeaders(context);
        if (token != null) {
            Logger.debug(token);
            AccessTokens user = AccessTokens.findOne(token);
            if (user != null && user.user.role.id == 1){
                context.request().username();
                return delegate.call(context);
            }
        }
        ObjectNode result = Json.newObject();
        result.put("error", "No authorizado");
        Result unauthorized = Results.unauthorized(result);
        return  CompletableFuture.completedFuture(unauthorized);
    }

    public static String getTokenFromHeaders(Http.Context context){
        String[] authTokenHeaderValues = context.request().headers().get("Authorization");
        if ((authTokenHeaderValues != null) && (authTokenHeaderValues.length == 1) && (authTokenHeaderValues[0] != null)) {
            return authTokenHeaderValues[0];
        }
        return null;
    }

}
