package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */

@Entity
public class Sizes extends Model {

    @Id
    public Long id;

    @NotNull
    @Column(unique = true)
    public String name;

    @UpdatedTimestamp
    public Date updatedAt;

    @CreatedTimestamp
    public Date createdAt;

    private static Finder<Long, Sizes> find(){
        return new Finder<>(Sizes.class);
    }

    public static List<Sizes> findAll(){
        return find().all();
    }

    public static Sizes findById(Long id){
        return find().byId(id);
    }
    
    public static Sizes update(Sizes updatedSize){
        updatedSize.update();
        return updatedSize;
    }
    
    public static Sizes create(String name){
        Sizes checkSizes = find().where().eq("name", name).findUnique();
        if (checkSizes == null){
            Sizes size = new Sizes();
            size.name = name;
            size.save();
            return size;
        }
        return null;
    }
    
}
