package models;

import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by Ariel Guzman on 6/4/2017
 */

@Entity
public class Addresses extends Model {

    @Id
    public Long id;

    public String phoneNumber;

    @NotNull
    public String addressLine;

    public String addressLine2;

    @NotNull
    public String city;

    @NotNull
    public String country;


    @UpdatedTimestamp
    public Date updatedAt;


    @CreatedTimestamp
    public Date createdAt;

    public static Finder<Long, Addresses> find(){
        return new Finder<>(Addresses.class);
    }
}
