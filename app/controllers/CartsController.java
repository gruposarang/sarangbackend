package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Carts;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class CartsController extends Controller {

    public Result findAll(){
        List<Carts> carts = Carts.findAll();
        return ok(Json.toJson(carts));
    }

    public Result findById(Long id){
        Carts cart = Carts.findById(id);
        if (cart == null){
            return notFound();
        } else {
            return ok(Json.toJson(cart));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addToCart(){

        JsonNode json = request().body().asJson();
        int quantity = json.findValue("quantity").asInt();
        Long productId = json.findValue("productId").asLong();

        if (productId == 0){
            ObjectNode result = Json.newObject();
            result.put("error", "Missing parameters");
            return badRequest(result);
        } else {
            Carts cart = Carts.addToCart(quantity,productId);
            if (cart != null){
                return ok(Json.toJson(cart));
            } else {
                ObjectNode result = Json.newObject();
                result.put("error", "No se pudo agregar el producto al carrito");
                return unauthorized(result);
            }

        }
    }
}
