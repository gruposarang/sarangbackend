package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.Logger;
import play.libs.Json;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import play.mvc.*;
import views.html.site.confirmation;
import views.html.site.confirmationfailure;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class UsersController extends Controller {
    private static String baseUrl = "http://sarangboutiquerd.com";

    @Inject
    MailerClient client;

    @Inject
    public UsersController(MailerClient mailer) {
        this.client = mailer;
    }

    public Result findAll(){
        List<Users> users = Users.findAll();
        return ok(Json.toJson(users));
    }

    public Result findById(Long id){
        Users user = Users.findById(id);
        if (user == null){
            return notFound();
        } else {
            return ok(Json.toJson(user));
        }
    }

    @With(SecuredAction.class)
    public Result checkUserType(){
        return ok();
    }

    public Result findMyOrders(){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        if (token != null){
            AccessTokens accessTokens = AccessTokens.findOne(token);
            if (accessTokens != null){
                List<Orders> orders = Orders.findByUser(accessTokens.user.id);
                return ok(Json.toJson(orders));
            }

        }
        return notFound();
    }



    public Result findMyCart (){
        Http.Context context = Http.Context.current();
        String token = SecuredAction.getTokenFromHeaders(context);
        if (token != null){
            Logger.info("toke");
            AccessTokens accessTokens = AccessTokens.findOne(token);
            if (accessTokens != null){
                Logger.info("user");

                Carts cart = Carts.findByUser(accessTokens.user.id);
                return ok(Json.toJson(cart));
            }
        }
        Logger.info(token);

        return notFound();
    }

    public Result logout(){
        Users.logout();
        return ok();
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").asText();
        String lastName = json.findPath("lastName").asText();
        String email = json.findPath("email").asText();
        String password = json.findPath("password").asText();
        JsonNode role = json.with("role");
        Long roleId = 2L;

        if (role != null){
            roleId =   role.get("id").asLong();
        }
        if (name == null || lastName == null || email == null || password == null){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {

            Users user = Users.create(name, lastName, email, password, roleId);

            if (user == null) {
                ObjectNode result = Json.newObject();
                result.put("error", "Correo ya existe");
                return unauthorized(result);
            } else {
            Confirmations confirmations = Confirmations.create(user);
                String url = baseUrl + "/confirm/" + confirmations.confirmToken;
                Email eMail = new Email();
                eMail.setSubject("Confirma tu cuenta en Sarang Boutique");
                eMail.setFrom("sarangboutiquerd@gmail.com");
                eMail.addTo(email);
                eMail.setBodyHtml("<div style=\"background-color: #EDEFF0; text-align: center\">" +
                        "<h3Confirma tu cuenta en Sarang Boutique</h3><br/><p>Hola, " + name +"</p><p> Una cuenta ha sido creada para ti en <a href=\""
                        + baseUrl +
                        "\">Sarang Boutique</a>. Solo tienes que hacer click <a href=\""  + url +"\">aqui</a> para confirmar tu cuenta y poder usar nuestros " +
                        "servicios, gracias.</p><p>Si la cuenta fue creada por error, por favor ignore este correo." +
                        "</div>");
                client.send(eMail);
                return ok(Json.toJson(user));
            }
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();
        String lastName = json.findPath("lastName").asText();

        Users user = Users.findById(id);
        if (user == null){
            return notFound();
        } else {
            if (!name.isEmpty()){
                user.name = name;
            }
            if (!lastName.isEmpty()) {
                user.lastName = lastName;
            }
            Users updatedUser = Users.update(user);
            return ok(Json.toJson(updatedUser));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result recoverPassword(){
        JsonNode json = request().body().asJson();

        String email = json.findPath("email").asText();

        if (email == null){
            return badRequest();
        } else {
            JsonNode user = Users.passwordRecover(email);
            if (user != null){
                String name = user.get("name").asText();
                String password = user.get("password").asText();
                Email eMail = new Email();
                eMail.setSubject("Recuperar Contraseña - Sarang Boutique");
                eMail.setFrom("sarangboutiquerd@gmail.com");
                eMail.addTo(email);
                eMail.setBodyHtml("<div style=\"background-color: #EDEFF0; text-align: center\">" +
                        "<h3>Hola " + name +",</h3><br/><p>Este correo es para informarle que ya" +
                        " se ha realizado su cambio de contraseña.<br/><br/></p>" +
                        "<h2>Contraseña: <span style=\"background-color: red; color: #fff\">"+password+"</span></h2><br/><br/>"+
                        "<p>Puede volver a <a href=\""
                        + baseUrl + "/login"+
                        "\">Iniciar sesion en Sarang Boutique</a></p>. <p>Gracias.</p>"+
                        "</div>");
                client.send(eMail);
                return ok(user);
            } else {
                ObjectNode result = Json.newObject();
                result.put("error", "No se pudo encontrar un usuario con este correo");
                return notFound(result);
            }

        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result login(){
        JsonNode json = request().body().asJson();
        String email = json.findPath("email").textValue();
        String password = json.findPath("password").textValue();
        if(email == null || password == null) {
            return badRequest("Missing parameter");
        } else {
            AccessTokens user = Users.login(email, password);
            if (user == null) {
                ObjectNode result = Json.newObject();
                result.put("error", "Correo o Email incorrecto");
                return unauthorized(result);
            } else if (!user.user.isMailVerified){
                ObjectNode result = Json.newObject();
                result.put("error", "Por favor verifique su correo antes de proceder");
                return unauthorized(result);
            } else {
                return ok(Json.toJson(user));
            }
        }
    }


    
     public Result confirmation(String token){
        Confirmations confirmations = Confirmations.byToken(token);
        if (confirmations != null){
            Users user = Users.findById(confirmations.user.id);
            user.status = Statuses.Active;
            user.isMailVerified = true;
            user.update();
            confirmations.delete();
            Email eMail = new Email();
            eMail.setSubject("Bienvenido a Sarang Boutique");
            eMail.setFrom("sarangboutiquerd@gmail.com");
            eMail.addTo(user.email);
            eMail.setBodyHtml("<div style=\"background-color: #EDEFF0; text-align: center\">" +
                    "<h3>Bienvenido a Sarang Boutique</h3><br/><p>Hola, " + user.name + "</p>. <p>Este correo es para decirte que tu cuenta en <a href=\""
                    + baseUrl +
                    "\">Sarang Boutique</a> ya esta activa</p>. <p>Gracias.</p>"+
                    "</div>");
            client.send(eMail);
            return ok(confirmation.render());
        } else {
            return ok(confirmationfailure.render());
        }
    }

}
