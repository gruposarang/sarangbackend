package controllers;

import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.admin.*;
import views.html.site.*;

import java.io.File;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(index.render());
    }

    public Result about() {
        return ok(about.render());
    }
    
    public Result contact() {
        return ok(contact.render());
    }

    public Result product() {
        return ok(product.render());
    }

    public Result product_detail(Long id) {
        return ok(single.render());
    }

    public Result checkout() {
        return ok(checkout.render());
    }

    public Result orders() {
        return ok(views.html.site.orders.render());
    }


    public Result login() {
        return ok(login.render());
    }

    public Result register() {
        return ok(register.render());
    }

    // Admin Site
    public Result admin_users(){
        return ok(users.render());
    }
    public Result admin_roles(){
        return ok(roles.render());
    }
    public Result admin_products() {
        return ok(products.render());
    }
    public Result admin_orders() {
        return ok(views.html.admin.orders.render());
    }
    public Result admin_colors(){
        return ok(colors.render());
    }
    public Result admin_sizes(){
        return ok(sizes.render());
    }
    public Result admin_discounts(){
        return ok(discounts.render());
    }
    public Result admin_categories(){
        return ok(categories.render());
    }

    public Result getPhoto(String path){
        String basePath = System.getProperty("user.dir").replace("target/universal/stage", "");
        Logger.info("PATH:::::::::::: "+path);
        Logger.info("BASEPATH:::::::::::: "+basePath);
        Logger.info("USER DIR::::"+System.getProperty("user.dir"));
        File image = new File(basePath + path);
        return ok(image);
    }
}
