package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Categories;
import models.Colors;
import models.Products;
import models.Sizes;
import net.coobird.thumbnailator.Thumbnails;
import play.Logger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class ProductsController extends Controller {
    ControllersHelper controllersHelper = new ControllersHelper();

    public Result findAll(){
        List<Products> products = Products.findAll();
        return ok(Json.toJson(products));
    }

    public Result findById(Long id){
        Products product = Products.findById(id);
        if (product == null){
            return notFound();
        } else {
            return ok(Json.toJson(product));
        }
    }

    public Result count(){
        int count = Products.count();
        ObjectNode node = Json.newObject();
        node.put("count", count);
        return ok(Json.toJson(node));
    }

    public Result between(int pageIndex, int pageSize){
        List<Products> products = Products.between(pageIndex, pageSize);
        return ok(Json.toJson(products));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").asText();
        float price = json.findPath("price").floatValue();
        String description = json.findPath("description").asText();
        int quantity = json.findPath("quantity").asInt();
        JsonNode category = json.with("category");
        JsonNode colors = json.withArray("colors");
        JsonNode sizes = json.withArray("sizes");

        Long categoryId = null;


        if (category != null){
            categoryId = category.get("id").asLong();
        }

        if (name == null || colors == null || sizes == null){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Products products = Products.create(name, price, description, quantity, categoryId,
                    colors, sizes);
            return ok(Json.toJson(products));

        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();
        float price = json.findPath("price").floatValue();
        String description = json.findPath("description").asText();
        int quantity = json.findPath("quantity").asInt();
        JsonNode category = json.with("category");
        JsonNode colors = json.withArray("colors");
        JsonNode sizes = json.withArray("sizes");

        Products product = Products.findById(id);
        if (product == null){
            return notFound();
        } else {
            if (name != null && colors != null && sizes != null){

                product.name = name;
                product.price = price;
                product.description = description;
                product.quantity = quantity;

                if (category != null){
                    product.category = Categories.findById(category.get("id").asLong());
                }
                for (int i = 0; i < colors.size(); i++) {
                    Colors color = Colors.findById(colors.get(i).findPath("id").asLong());
                    product.colors.add(color);
                }

                for (int i = 0; i < sizes.size(); i++) {
                    Sizes size = Sizes.findById(sizes.get(i).findPath("id").asLong());
                    product.sizes.add(size);
                }
            }
            Products.update(product);
            return ok(Json.toJson(product));
        }

    }


    public Result imageUpload(long productId) {
        play.mvc.Http.MultipartFormData<File> body = request().body().asMultipartFormData();
        play.mvc.Http.MultipartFormData.FilePart<File> picture = body.getFile("picture");
        if (picture != null) {
            String fileName = "photo";
            File file = picture.getFile();

            String basePath = System.getProperty("user.dir").replace("target/universal/stage", "");
           Logger.info("BASEPATH:::::::::::: "+basePath);    
           
            File folder = new File(basePath + "images/products/" + productId);
            boolean isFolder = folder.mkdirs();
            if (isFolder || folder.exists()) {
                String extension = picture.getContentType().replace("image/", "");
                File bfile = new File(basePath + "images/products/" + productId + "/" + fileName + "." +  extension);

                File newFile = controllersHelper.fileReplace(file, bfile);
                String thumbnail = "";
                try {
                    File thumbFile = new File(basePath + "images/products/" + productId + "/thumbnail" + "." +  extension);
                    Thumbnails.of(newFile)
                            .size(233, 323)
                            .toFile(thumbFile);
                    thumbnail = request().host() + "/" + thumbFile.getPath().replace("\\", "/").replace("/opt/sarangbackend", "public");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String path = request().host() + "/" + newFile.getPath().replace("\\", "/").replace("/opt/sarangbackend", "public");
                Products product = Products.addPhoto(productId, path, thumbnail);
                return ok(Json.toJson(product));
            } else {
                Logger.info("There is file");
                return badRequest(controllersHelper.fileNotFound());
            }
        } else {
            Logger.info("There isnt file");
            return badRequest(controllersHelper.fileMissing());
        }
    }
}
