package controllers;

import com.fasterxml.jackson.databind.node.ObjectNode;
import play.libs.Json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

/**
 * Created by Ariel Guzman on 7/23/2017
 */
public class ControllersHelper {

    public File fileReplace(File oldFile, File newFile){
        FileInputStream inStream;
        FileOutputStream outStream;
        try {
            inStream = new FileInputStream(oldFile);
            outStream = new FileOutputStream(newFile);
            byte[] buffer = new byte[1024];
            int length;
            //copy the file content in bytes
            while ((length = inStream.read(buffer)) > 0) {
                outStream.write(buffer, 0, length);
            }
            inStream.close();
            outStream.close();
            //delete the original file
            oldFile.delete();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }

        return newFile;
    }

    public ObjectNode fileNotFound(){
        ObjectNode response = Json.newObject();
        response.put("type", "error");
        response.put("statusCode", 500);
        response.put("message","We couldn't find the requested file");

        return response;
    }


    public ObjectNode fileMissing(){
        ObjectNode response = Json.newObject();
        response.put("type", "error");
        response.put("statusCode", 500);
        response.put("message", "Missing file");
        return response;
    }

}