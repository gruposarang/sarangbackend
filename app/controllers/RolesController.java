package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Roles;
import play.libs.Json;
import play.libs.mailer.MailerClient;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class RolesController extends Controller {

    public Result findAll(){
        List<Roles> roles = Roles.findAll();
        return ok(Json.toJson(roles));
    }

    public Result findById(Long id){
        Roles role = Roles.findById(id);
        if (role == null){
            return notFound();
        } else {
            return ok(Json.toJson(role));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").asText();

        if (name == null ){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Roles role =  Roles.create(name);

            if (role == null) {
                ObjectNode result = Json.newObject();
                result.put("error", "Role already exists");
                return unauthorized(result);
            } else {
                return ok(Json.toJson(role));
            }
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();

        Roles role =  Roles.findById(id);

        if (role == null){
            return notFound();
        } else {
            if (!name.isEmpty()){
                role.name = name;
            }
            Roles.update(role);
            return ok(Json.toJson(role));
        }
    }
}
