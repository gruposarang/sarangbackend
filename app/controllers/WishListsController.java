package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.WishLists;
import play.Logger;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class WishListsController extends Controller {

    public Result findAll(){
        List<WishLists> wishLists = WishLists.findAll();
        return ok(Json.toJson(wishLists));
    }

    public Result findById(Long id){
        WishLists wishList = WishLists.findById(id);
        if (wishList == null){
            return notFound();
        } else {
            return ok(Json.toJson(wishList));
        }
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result addToWishList(){

        JsonNode json = request().body().asJson();
        Long productId = json.findValue("productId").asLong();

        if (productId == 0){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            WishLists wishList = WishLists.addToWishList(productId);
            if (wishList != null){
                Logger.info("added");
                return ok(Json.toJson(wishList));
            } else {
                ObjectNode result = Json.newObject();
                result.put("Error", "No se pudo agregar el producto al WishList");
                return badRequest(result);
            }

        }
    }
}
