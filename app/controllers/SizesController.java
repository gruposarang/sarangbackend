package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */
public class SizesController extends Controller {

    public Result findAll(){
        List<Sizes> sizes = Sizes.findAll();
        return ok(Json.toJson(sizes));
    }

    public Result findById(Long id){
        Sizes size = Sizes.findById(id);
        if (size == null){
            return notFound();
        } else {
            return ok(Json.toJson(size));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").asText();

        if (name == null){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Sizes size = Sizes.create(name);
            return ok(Json.toJson(size));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();

        Sizes size = Sizes.findById(id);

        if (size == null){
            return notFound();
        } else {
            if (!name.isEmpty()){
                size.name = name;
            }
            Sizes.update(size);
            return ok(Json.toJson(size));
        }
    }
}
