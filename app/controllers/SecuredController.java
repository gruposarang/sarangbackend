package controllers;

import models.SecuredAction;
import play.mvc.Controller;
import play.mvc.With;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
@With(SecuredAction.class)
public abstract class SecuredController extends Controller {
}
