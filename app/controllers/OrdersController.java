package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Orders;
import models.Products;
import models.Statuses;
import play.libs.Json;
import play.libs.mailer.Email;
import play.libs.mailer.MailerClient;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Ariel Guzman on 5/31/2017
 */
public class OrdersController extends Controller {
    @Inject
    MailerClient client;

    @Inject
    public OrdersController(MailerClient mailer) {
        this.client = mailer;
    }
    
    public Result findAll(){
        List<Orders> orders = Orders.findAll();
        return ok(Json.toJson(orders));
    }

    public Result findById(Long id){
        Orders order = Orders.findById(id);
        if (order == null){
            return notFound();
        } else {
            return ok(Json.toJson(order));
        }
    }

    public Result count(){
        int count = Products.count();
        ObjectNode node = Json.newObject();
        node.put("count", count);
        return ok(Json.toJson(node));
    }

    public Result between(int pageIndex, int pageSize){
        List<Orders> orders = Orders.between(pageIndex, pageSize);
        return ok(Json.toJson(orders));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
    
        JsonNode json = request().body().asJson();
        String note = json.findPath("note").asText();

        if (note == null){
            ObjectNode result = Json.newObject();
            result.put("error", "Missing parameters");
            return badRequest(result);
        } else {
            Orders order = Orders.create(note);
            if (order != null){
                String baseUrl = "http://sarangboutiquerd.com";
                Email eMail = new Email();
                eMail.setSubject("Tu compra en SarangBoutiqueRD.com");
                eMail.setFrom("sarangboutiquerd@gmail.com");
                eMail.addTo(order.user.email);
                eMail.setBodyHtml("<div style=\"background-color: #EDEFF0; text-align: center\">" +
                        "<h3Hola "+order.user.name + "</h3>,<br/><p>Gracias por comprar con nosotros en <a href=\""
                        + baseUrl +
                        "\">Sarang Boutique</a>. Para proceder con tu orden envia una " +
                        "transferencia a la cuenta 794041301 del Banco Popular a nombre de Renelis Abreu y responde a este correo con el recibo<br/>" +
                        "Puedes ver los detalles de tu orden <a href=\"" + baseUrl + "/ordenes\">Aqui</a>" +
                        "</p><p>Esperamos que vuelva a comprar con nosotros." +
                        "</div>");
                client.send(eMail);
                return ok(Json.toJson(order));
            } else {
                ObjectNode result = Json.newObject();
                result.put("error", "La orden no pudo ser completada.");
                return badRequest(result);
            }

        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result markAsPaid(){
        JsonNode json = request().body().asJson();
        Long orderId = json.findPath("orderId").asLong();

        if (orderId == 0){
            ObjectNode result = Json.newObject();
            result.put("error", "Missing parameters");
            return badRequest(result);
        } else {
            Orders order = Orders.changeStatus(orderId, Statuses.Pagada);
            sendStatusChangeEmail(order);
            return ok(Json.toJson(order));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result markAsDelivered(){
        JsonNode json = request().body().asJson();
        Long orderId = json.findPath("orderId").asLong();

        if (orderId == 0){
            ObjectNode result = Json.newObject();
            result.put("error", "Missing parameters");
            return badRequest(result);
        } else {
            Orders order = Orders.changeStatus(orderId, Statuses.Entregada);
            sendStatusChangeEmail(order);
            return ok(Json.toJson(order));
        }
    }
    
    public void sendStatusChangeEmail(Orders order){
        String baseUrl = "http://sarangboutiquerd.com";
        Email eMail = new Email();
        eMail.setSubject("Tu compra en SarangBoutiqueRD.com");
        eMail.setFrom("sarangboutiquerd@gmail.com");
        eMail.addTo(order.user.email);
        eMail.setBodyHtml("<div style=\"background-color: #EDEFF0; text-align: center\">" +
                "<h3Hola "+order.user.name + "</h3>,<br/><p>El estado de tu orden ha cambiado. Puedes ver los detalles de tu orden <a href=\"" + baseUrl + "/ordenes\">Aqui</a>" +
                "</p><p>Esperamos que vuelva a comprar con nosotros." +
                "</div>");
        client.send(eMail);
    }
}
