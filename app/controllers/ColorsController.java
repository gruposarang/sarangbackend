package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.*;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */
public class ColorsController extends Controller {

    public Result findAll(){
        List<Colors> colors = Colors.findAll();
        return ok(Json.toJson(colors));
    }

    public Result findById(Long id){
        Colors color = Colors.findById(id);
        if (color == null){
            return notFound();
        } else {
            return ok(Json.toJson(color));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String hex = json.findPath("hex").asText();
        String name = json.findPath("name").asText();

        if (name == null || hex == null){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Colors color = Colors.create(name, hex);
            return ok(Json.toJson(color));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();
        String hex = json.findPath("hex").asText();

        Colors color = Colors.findById(id);

        if (color == null){
            return notFound();
        } else {
            if (!name.isEmpty() || hex.isEmpty()){
                color.name = name;
            }
            Colors.update(color);
            return ok(Json.toJson(color));
        }
    }
}