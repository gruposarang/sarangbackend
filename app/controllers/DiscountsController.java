package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Discounts;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */
public class DiscountsController extends Controller {

    public Result findAll(){
        List<Discounts> discounts = Discounts.findAll();
        return ok(Json.toJson(discounts));
    }

    public Result findById(Long id){
        Discounts discount = Discounts.findById(id);
        if (discount == null){
            return notFound();
        } else {
            return ok(Json.toJson(discount));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result addDiscount(){
        JsonNode json = request().body().asJson();
        JsonNode product = json.with("product");
        float percentage = json.findPath("percentage").floatValue();

        if (product == null || percentage == 0){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Discounts discount = Discounts.addDiscount(product, percentage);
            return ok(Json.toJson(discount));
        }
    }
}