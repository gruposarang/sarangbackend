package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Categories;
import play.libs.Json;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.List;

/**
 * Created by Ariel Guzman on 7/19/2017
 */
public class CategoriesController extends Controller {

    public Result findAll(){
        List<Categories> categories = Categories.findAll();
        return ok(Json.toJson(categories));
    }

    public Result findById(Long id){
        Categories category = Categories.findById(id);
        if (category == null){
            return notFound();
        } else {
            return ok(Json.toJson(category));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result create(){
        JsonNode json = request().body().asJson();
        String name = json.findPath("name").asText();

        if (name == null){
            ObjectNode result = Json.newObject();
            result.put("Error", "Missing parameters");
            return badRequest(result);
        } else {
            Categories category = Categories.create(name);
            return ok(Json.toJson(category));
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result update(Long id){
        JsonNode json = request().body().asJson();

        String name = json.findPath("name").asText();

        Categories category = Categories.findById(id);

        if (category == null){
            return notFound();
        } else {
            if (!name.isEmpty()){
                category.name = name;
            }
            Categories.update(category);
            return ok(Json.toJson(category));
        }
    }
}