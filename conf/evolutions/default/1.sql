# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table access_tokens (
  id                            bigint not null,
  token                         varchar(255) not null,
  user_id                       bigint not null,
  ttl                           integer not null,
  created_at                    timestamp not null,
  updated_at                    timestamp not null,
  constraint pk_access_tokens primary key (id)
);
create sequence access_tokens_seq;

create table addresses (
  id                            bigint not null,
  phone_number                  varchar(255),
  address_line                  varchar(255) not null,
  address_line2                 varchar(255),
  city                          varchar(255) not null,
  country                       varchar(255) not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint pk_addresses primary key (id)
);
create sequence addresses_seq;

create table carts (
  id                            bigint not null,
  user_id                       bigint not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint uq_carts_user_id unique (user_id),
  constraint pk_carts primary key (id)
);
create sequence carts_seq;

create table carts_products (
  id                            bigint not null,
  product_id                    bigint not null,
  cart_id                       bigint not null,
  quantity                      integer not null,
  constraint pk_carts_products primary key (id)
);
create sequence carts_products_seq;

create table categories (
  id                            bigint not null,
  name                          varchar(255) not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint pk_categories primary key (id)
);
create sequence categories_seq;

create table colors (
  id                            bigint not null,
  name                          varchar(255) not null,
  hex                           varchar(255) not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint uq_colors_name unique (name),
  constraint pk_colors primary key (id)
);
create sequence colors_seq;

create table confirmations (
  id                            bigint not null,
  confirm_token                 varchar(255) not null,
  user_id                       bigint not null,
  constraint pk_confirmations primary key (id)
);
create sequence confirmations_seq;

create table discounts (
  id                            bigint not null,
  product_id                    bigint not null,
  percentage                    float,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint uq_discounts_product_id unique (product_id),
  constraint pk_discounts primary key (id)
);
create sequence discounts_seq;

create table historical_prices (
  id                            bigint not null,
  product_id                    bigint not null,
  order_id                      bigint not null,
  price                         float not null,
  quantity                      integer not null,
  constraint pk_historical_prices primary key (id)
);
create sequence historical_prices_seq;

create table orders (
  id                            bigint not null,
  is_pick_up                    boolean not null,
  address_id                    bigint,
  status                        integer not null,
  note                          TEXT,
  user_id                       bigint not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint ck_orders_status check (status in (0,1,2,3,4)),
  constraint pk_orders primary key (id)
);
create sequence orders_seq;

create table products (
  id                            bigint not null,
  name                          varchar(255) not null,
  price                         float not null,
  description                   TEXT,
  quantity                      integer,
  status                        integer,
  photo                         varchar(255),
  photo_thumbnail               varchar(255),
  category_id                   bigint not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint ck_products_status check (status in (0,1,2,3,4)),
  constraint pk_products primary key (id)
);
create sequence products_seq;

create table products_colors (
  products_id                   bigint not null,
  colors_id                     bigint not null,
  constraint pk_products_colors primary key (products_id,colors_id)
);

create table products_sizes (
  products_id                   bigint not null,
  sizes_id                      bigint not null,
  constraint pk_products_sizes primary key (products_id,sizes_id)
);

create table roles (
  id                            bigint not null,
  name                          varchar(255) not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint pk_roles primary key (id)
);
create sequence roles_seq;

create table sizes (
  id                            bigint not null,
  name                          varchar(255) not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint uq_sizes_name unique (name),
  constraint pk_sizes primary key (id)
);
create sequence sizes_seq;

create table users (
  id                            bigint not null,
  name                          varchar(255) not null,
  last_name                     varchar(255) not null,
  email                         varchar(255) not null,
  password                      varchar(255) not null,
  is_mail_verified              boolean not null,
  status                        integer,
  role_id                       bigint,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint ck_users_status check (status in (0,1,2,3,4)),
  constraint uq_users_email unique (email),
  constraint pk_users primary key (id)
);
create sequence users_seq;

create table wish_lists (
  id                            bigint not null,
  user_id                       bigint not null,
  updated_at                    timestamp not null,
  created_at                    timestamp not null,
  constraint uq_wish_lists_user_id unique (user_id),
  constraint pk_wish_lists primary key (id)
);
create sequence wish_lists_seq;

create table wish_lists_products (
  wish_lists_id                 bigint not null,
  products_id                   bigint not null,
  constraint pk_wish_lists_products primary key (wish_lists_id,products_id)
);

alter table access_tokens add constraint fk_access_tokens_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_access_tokens_user_id on access_tokens (user_id);

alter table carts add constraint fk_carts_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;

alter table carts_products add constraint fk_carts_products_product_id foreign key (product_id) references products (id) on delete restrict on update restrict;
create index ix_carts_products_product_id on carts_products (product_id);

alter table carts_products add constraint fk_carts_products_cart_id foreign key (cart_id) references carts (id) on delete restrict on update restrict;
create index ix_carts_products_cart_id on carts_products (cart_id);

alter table confirmations add constraint fk_confirmations_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_confirmations_user_id on confirmations (user_id);

alter table discounts add constraint fk_discounts_product_id foreign key (product_id) references products (id) on delete restrict on update restrict;

alter table historical_prices add constraint fk_historical_prices_product_id foreign key (product_id) references products (id) on delete restrict on update restrict;
create index ix_historical_prices_product_id on historical_prices (product_id);

alter table historical_prices add constraint fk_historical_prices_order_id foreign key (order_id) references orders (id) on delete restrict on update restrict;
create index ix_historical_prices_order_id on historical_prices (order_id);

alter table orders add constraint fk_orders_address_id foreign key (address_id) references addresses (id) on delete restrict on update restrict;
create index ix_orders_address_id on orders (address_id);

alter table orders add constraint fk_orders_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;
create index ix_orders_user_id on orders (user_id);

alter table products add constraint fk_products_category_id foreign key (category_id) references categories (id) on delete restrict on update restrict;
create index ix_products_category_id on products (category_id);

alter table products_colors add constraint fk_products_colors_products foreign key (products_id) references products (id) on delete restrict on update restrict;
create index ix_products_colors_products on products_colors (products_id);

alter table products_colors add constraint fk_products_colors_colors foreign key (colors_id) references colors (id) on delete restrict on update restrict;
create index ix_products_colors_colors on products_colors (colors_id);

alter table products_sizes add constraint fk_products_sizes_products foreign key (products_id) references products (id) on delete restrict on update restrict;
create index ix_products_sizes_products on products_sizes (products_id);

alter table products_sizes add constraint fk_products_sizes_sizes foreign key (sizes_id) references sizes (id) on delete restrict on update restrict;
create index ix_products_sizes_sizes on products_sizes (sizes_id);

alter table users add constraint fk_users_role_id foreign key (role_id) references roles (id) on delete restrict on update restrict;
create index ix_users_role_id on users (role_id);

alter table wish_lists add constraint fk_wish_lists_user_id foreign key (user_id) references users (id) on delete restrict on update restrict;

alter table wish_lists_products add constraint fk_wish_lists_products_wish_lists foreign key (wish_lists_id) references wish_lists (id) on delete restrict on update restrict;
create index ix_wish_lists_products_wish_lists on wish_lists_products (wish_lists_id);

alter table wish_lists_products add constraint fk_wish_lists_products_products foreign key (products_id) references products (id) on delete restrict on update restrict;
create index ix_wish_lists_products_products on wish_lists_products (products_id);


# --- !Downs

alter table access_tokens drop constraint if exists fk_access_tokens_user_id;
drop index if exists ix_access_tokens_user_id;

alter table carts drop constraint if exists fk_carts_user_id;

alter table carts_products drop constraint if exists fk_carts_products_product_id;
drop index if exists ix_carts_products_product_id;

alter table carts_products drop constraint if exists fk_carts_products_cart_id;
drop index if exists ix_carts_products_cart_id;

alter table confirmations drop constraint if exists fk_confirmations_user_id;
drop index if exists ix_confirmations_user_id;

alter table discounts drop constraint if exists fk_discounts_product_id;

alter table historical_prices drop constraint if exists fk_historical_prices_product_id;
drop index if exists ix_historical_prices_product_id;

alter table historical_prices drop constraint if exists fk_historical_prices_order_id;
drop index if exists ix_historical_prices_order_id;

alter table orders drop constraint if exists fk_orders_address_id;
drop index if exists ix_orders_address_id;

alter table orders drop constraint if exists fk_orders_user_id;
drop index if exists ix_orders_user_id;

alter table products drop constraint if exists fk_products_category_id;
drop index if exists ix_products_category_id;

alter table products_colors drop constraint if exists fk_products_colors_products;
drop index if exists ix_products_colors_products;

alter table products_colors drop constraint if exists fk_products_colors_colors;
drop index if exists ix_products_colors_colors;

alter table products_sizes drop constraint if exists fk_products_sizes_products;
drop index if exists ix_products_sizes_products;

alter table products_sizes drop constraint if exists fk_products_sizes_sizes;
drop index if exists ix_products_sizes_sizes;

alter table users drop constraint if exists fk_users_role_id;
drop index if exists ix_users_role_id;

alter table wish_lists drop constraint if exists fk_wish_lists_user_id;

alter table wish_lists_products drop constraint if exists fk_wish_lists_products_wish_lists;
drop index if exists ix_wish_lists_products_wish_lists;

alter table wish_lists_products drop constraint if exists fk_wish_lists_products_products;
drop index if exists ix_wish_lists_products_products;

drop table if exists access_tokens;
drop sequence if exists access_tokens_seq;

drop table if exists addresses;
drop sequence if exists addresses_seq;

drop table if exists carts;
drop sequence if exists carts_seq;

drop table if exists carts_products;
drop sequence if exists carts_products_seq;

drop table if exists categories;
drop sequence if exists categories_seq;

drop table if exists colors;
drop sequence if exists colors_seq;

drop table if exists confirmations;
drop sequence if exists confirmations_seq;

drop table if exists discounts;
drop sequence if exists discounts_seq;

drop table if exists historical_prices;
drop sequence if exists historical_prices_seq;

drop table if exists orders;
drop sequence if exists orders_seq;

drop table if exists products;
drop sequence if exists products_seq;

drop table if exists products_colors;

drop table if exists products_sizes;

drop table if exists roles;
drop sequence if exists roles_seq;

drop table if exists sizes;
drop sequence if exists sizes_seq;

drop table if exists users;
drop sequence if exists users_seq;

drop table if exists wish_lists;
drop sequence if exists wish_lists_seq;

drop table if exists wish_lists_products;

