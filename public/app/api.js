angular.module("sarangApi", [
        "ngResource",
        "ngStorage"
    ])
    .run(function($http, $localStorage){
        $http.defaults.headers.common.Authorization = $localStorage.token;
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('ngInterceptor');
    })
    .factory('ngInterceptor', function($q, $window, $injector, BASE_URL, $localStorage) {
        return {
            'request': function(config) {
                console.log(config)
                var $http = $injector.get("$http");
                $http.defaults.headers.common.Authorization = $localStorage.token;
                return config;
            },
            // optional method
            'requestError': function(rejection) {
                // do something on error
                return $q.reject(rejection);
            },



            // optional method
            'response': function(response) {
                // do something on success
                console.log(response);
                return response;
            },

            // optional method
            'responseError': function(rejection) {
                if (rejection.status == 401) {
                    location.replace(BASE_URL + "login");
                }
                // do something on error
                return $q.reject(rejection);
            }
        };
    })
    .constant("BASE_URL", "/")
    .factory("Users", function($resource, $localStorage, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/users',
                method: 'GET',
                isArray: true
            },
            findMyCart: {
                url: BASE_URL + 'api/users/my-account/cart',
                method: 'GET'
            },
            findMyOrders: {
                url: BASE_URL + 'api/users/my-account/orders',
                method: 'GET',
                isArray: true
            },
            checkUserType: {
                url: BASE_URL + 'api/users/my-account/checkUserType',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/users",
                method: 'POST'
            },
            login: {
                url: BASE_URL +  'api/users/login',
                method: 'POST',
                interceptor: {
                    response: function(response){
                        $localStorage.token = response.resource.token;
                        $localStorage.user = response.resource.user;
                        $localStorage.id = response.resource.user.id;
                        return response.resource;
                    }
                }
            },
            logout: {
                url: BASE_URL + 'api/users/logout',
                method: 'POST'
            }
        })
    })
    .factory("Roles", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/roles',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/roles/:roleId',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/roles",
                method: 'POST'
            }
        })
    })
    .factory("Products", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/products',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/products/:productId',
                method: 'GET'
            },
            uploadFile: {
                url: BASE_URL + 'api/products/:productId/upload',
                method: 'PUT'
            },
            create: {
                url: BASE_URL + "api/products",
                method: 'POST'
            }
        })
    })
    .factory("Sizes", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/sizes',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/sizes/:sizeId',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/sizes",
                method: 'POST'
            },
            update: {
                url: BASE_URL + "api/sizes/:sizeId",
                method: "PUT"
            }
        })
    })
    .factory("Categories", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/categories',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/categories/:categoryId',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/categories",
                method: 'POST'
            },
            update: {
                url: BASE_URL + "api/categories/:categoryId",
                method: "PUT"
            }
        })
    })
    .factory("Colors", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/colors',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/colors/:colorId',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/colors",
                method: 'POST'
            },
            update: {
                url: BASE_URL + "api/colors/:colorId",
                method: "PUT"
            }
        })
    })
    .factory("WishLists", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/wishLists',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/wishLists/:wishListId',
                method: 'GET'
            },
            addToWishList: {
                url: BASE_URL + "api/wishLists",
                method: 'POST'
            }
        })
    })
    .factory("Carts", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/carts',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/carts/:cartId',
                method: 'GET'
            },
            addToCart: {
                url: BASE_URL + "api/carts",
                method: 'POST'
            }
        })
    })
    .factory("Discounts", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/discounts',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/discounts/:discountId',
                method: 'GET'
            },
            addDiscount: {
                url: BASE_URL + "api/discounts",
                method: 'POST'
            }
        })
    })
    .factory("Orders", function($resource, BASE_URL){
        return $resource(null, null, {
            findAll: {
                url: BASE_URL + 'api/orders',
                method: 'GET',
                isArray: true
            },
            findById:{
                url: BASE_URL + 'api/orders/:orderId',
                method: 'GET'
            },
            create: {
                url: BASE_URL + "api/orders",
                method: 'POST'
            },
            update: {
                url: BASE_URL + "api/orders/:orderId",
                method: "PUT"
            },
            markAsPaid: {
                url: BASE_URL + "api/orders/paid",
                method: "PUT"
            },
            markAsDelivered: {
                url: BASE_URL + "api/orders/delivered",
                method: "PUT"
            }
        })
    });