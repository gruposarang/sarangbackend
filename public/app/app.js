angular.module("sarangApp", [
        "sarangApi",
        "ui.bootstrap"
    ])
    .directive('fileModel', ['$parse', function ($parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                var model = $parse(attrs.fileModel);
                var modelSetter = model.assign;

                element.bind('change', function(){
                    scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                    });
                });
            }
        };
    }])
    .controller("MainController", function($localStorage, $scope, BASE_URL, Users){
        $scope.$storage = $localStorage;

        $scope.logout = function(){
            Users.logout(function(){
                location.replace(BASE_URL);
                $localStorage.$reset();
            })
        }
    })
    .controller("LoginController", function(Users,$scope, BASE_URL){
        $scope.user = {
            email: "",
            password: ""
        };

        $scope.error = "";

        $scope.doLogin = function(){

            Users.login($scope.user, function(success){
                location.replace(BASE_URL)
            }, function(req){
                $scope.error = req.data.error;
            });
        };
    })
    .controller("RegisterController", function($scope, Users, Roles, BASE_URL) {
        Roles.findAll(function(roles){
            $scope.user = {
                name: "",
                lastName: "",
                email: "",
                password: "",
                role: roles[1]
            };
        });

        $scope.error = "";

        $scope.register = function(){
            if ($scope.user.email !== ""){
                Users.create($scope.user, function(succes){
                    location.replace(BASE_URL + "login");
                }, function(req){
                    $scope.error = req.data.error;
                });
            }
        }
    })
    .controller("ProductsController", function($scope, Products, Categories) {
        Products.findAll(function(products){
            $scope.products = products;
        });

        Categories.findAll(function(categories){
            $scope.categories = categories;
        })

    })
    .controller("CartsController", function($scope, Users, Orders) {
        //Products.findAll(function(products){
        //    $scope.products = products;
        //});

        function findCart(){
            Users.findMyCart(function(cart){
                $scope.cart = cart;
            });
        }


        $scope.createOrder = function(){
            Orders.create({note: ""}, function(){
                alert("Se ha creado una nueva orden");
                findCart();
            }, function(req){
                alert(req.data.error);
            })
        };

        findCart();

    })
    .controller("OrdersController", function($scope, Users) {
        //Products.findAll(function(products){
        //    $scope.products = products;
        //});
        $scope.orders = [];

        Users.findMyOrders(function(orders){
            orders.forEach(function(item){
                item.total = 0;
                item.historicalPrices.forEach(function(detail){
                    item.total += detail.price * detail.quantity;
                });
                $scope.orders.push(item);
            });
        });

    })
    .controller("ProductDetailController", function($scope, $location, Products, Categories, Carts) {
        var id = $location.absUrl().split("/")[4];
        Products.findById({productId: id}, function(product){
            $scope.product = product;
        });

        Categories.findAll(function(categories){
            $scope.categories = categories;
        });

        $scope.addToCart = function(){
            var quantity = document.getElementById("productQuantity").innerHTML.replace("<span>", "").replace("</span>", "");
            Carts.addToCart({productId: id, quantity: quantity}, function(response){
                alert("Agregado al Carrito exitosamente!");
            }, function(req){
                alert(req.data.error);
            })
        };
        //$scope.addToWishList = function(){
        //   WishLists.addToWishList({productId: id}, function(response){
        //     alert("Agregado al WishList");
        //   })
        //}

    })
    //ADMIN
    .controller("UsersAdminController", function($scope, Users, BASE_URL, $uibModal){

        function getUsers(){
            Users.findAll(function(roles){
                $scope.users = roles;
            });
        }
        Users.checkUserType(function(){
            getUsers();
        });

        $scope.open = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Usuario</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createUser()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="user.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Apellido</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="user.lastName" required type="text" class="form-control" id="inputName" placeholder="Apellido">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Email</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="user.email" required type="email" class="form-control" id="inputName" placeholder="Email">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Contrase�a</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="user.password" required type="password" class="form-control" id="inputName" placeholder="Contrase�a">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Role</label>'+
                '<div class="col-sm-10">'+
                '<select class="form-control" ng-options="role.name for role in roles track by role.id" ng-model="user.role">'+
                '</select>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',

                controller: 'UsersModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function () {
                getUsers();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("OrdersAdminController", function($scope, Orders, Users, $uibModal){
        $scope.orders = [];
        function getOrders(){
            Orders.findAll(function(orders){
                orders.forEach(function(item){
                    item.total = 0;
                    item.historicalPrices.forEach(function(detail){
                        item.total += detail.price * detail.quantity;
                    });
                $scope.orders.push(item);
            });
            });
        }

        $scope.markAsPaid = function(id){
            Orders.markAsPaid({orderId: id}, function(response){
                getOrders();
            })
        };

        $scope.markAsDelivered = function(id){
            Orders.markAsDelivered({orderId: id}, function(response){
                getOrders();
            })
        };
        Users.checkUserType(function(){
            getOrders();
        });
        


    })
    .controller("RolesAdminController", function($scope, Users, Roles, BASE_URL, $uibModal){
        
        function getRoles(){
            Roles.findAll(function(roles){
                $scope.roles = roles;
            });
        }
        
        Users.checkUserType(function(){
            getRoles();
        });
        

        $scope.openCreateRole = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Role</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createRole()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="role.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'RolesModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getRoles();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("ProductsAdminController", function($scope, Products, BASE_URL, $uibModal, Users){
        
        function getProducts(){
            Products.findAll(function(products){
                $scope.products = products;
            });
        }

        Users.checkUserType(function(){
            getProducts();
        });

        $scope.openCreateProduct = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Producto</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createProduct()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="product.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Foto</label>'+
                '<div class="col-sm-10">'+
                '<input file-model="file" required type="file" class="form-control" id="inputName" accept="image/*">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Precio</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="product.price" required type="number" step="0.01" min="0" class="form-control" id="inputName" placeholder="Precio">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Descripcion</label>'+
                '<div class="col-sm-10">'+
                '<textarea ng-model="product.description" class="form-control" id="inputName" placeholder="Descripcion"></textarea>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Cantidad</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="product.quantity" required type="number" min="0" class="form-control" id="inputName" placeholder="Cantidad">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Categoria</label>'+
                '<div class="col-sm-10">'+
                '<select class="form-control" ng-options="category.name for category in categories track by category.id" ng-model="product.category">'+
                '</select>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Sizes</label>'+
                '<div class="col-sm-10">'+
                '<select class="form-control" multiple ng-options="size.name for size in sizes track by size.id" ng-model="product.sizes">'+
                '</select>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Colores</label>'+
                '<div class="col-sm-10">'+
                '<select class="form-control" multiple ng-options="color.name for color in colors track by color.id" ng-model="product.colors">'+
                '</select>'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'ProductsModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getProducts();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("SizesAdminController", function($scope, Sizes, BASE_URL, $uibModal, Users){
        
        function getSizes(){
            Sizes.findAll(function(sizes){
                $scope.sizes = sizes;
            });
        }
        
        Users.checkUserType(function(){
            getSizes();
        });

        $scope.openCreateSize = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Size</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createSize()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="size.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'SizesModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getSizes();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("CategoriesAdminController", function($scope, Categories, BASE_URL, $uibModal, Users){
        
        function getCategories(){
            Categories.findAll(function(categories){
                $scope.categories = categories;
            });
        }
        
        Users.checkUserType(function(){
            getCategories();        
        });

        $scope.openCreateCategory = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nueva Categoria</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createCategory()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="category.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'CategoriesModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getCategories();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("DiscountsAdminController", function($scope, Discounts, BASE_URL, $uibModal, Users){
        
        function getDiscounts(){
            Discounts.findAll(function(discounts){
                $scope.discounts = discounts;
            });
        }
        
        Users.checkUserType(function(){
            getDiscounts();
        });

        $scope.openCreateColor = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Color</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createColor()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="color.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Hex</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="color.hex" required type="text" class="form-control" id="inputName" placeholder="Hex">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'ColorsModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getDiscounts();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller("ColorsAdminController", function($scope, Colors, BASE_URL, $uibModal, Users){
        
        function getColors(){
            Colors.findAll(function(colors){
                $scope.colors = colors;
            });
        }
        
        Users.checkUserType(function(){
            getColors();
        });

        $scope.openCreateColor = function () {
            var modalInstance = $uibModal.open({
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                template:
                '<div class="modal-header">'+
                '<h3 class="modal-title" id="modal-title">Nuevo Color</h3>'+
                '</div>'+
                '<div class="modal-body" id="modal-body">'+
                '<form class="form-horizontal" ng-submit="createColor()">'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Nombre</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="color.name" required type="text" class="form-control" id="inputName" placeholder="Nombre">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<label for="inputName" class="col-sm-2 control-label">Hex</label>'+
                '<div class="col-sm-10">'+
                '<input ng-model="color.hex" required type="text" class="form-control" id="inputName" placeholder="Hex">'+
                '</div>'+
                '</div>'+
                '<div class="form-group">'+
                '<div class="col-sm-offset-2 col-sm-10">'+
                '<button type="submit" class="btn btn-default">Crear</button>'+
                '</div>'+
                '</div>'+
                '</form>'+
                '</div>',
                controller: 'ColorsModalInstanceCtrl',
                size: 'md'
            });

            modalInstance.result.then(function (selectedItem) {
                getColors();
            }, function () {
                //$log.info('Modal dismissed at: ' + new Date());
            });
        };

    })
    .controller('UsersModalInstanceCtrl', function ($uibModalInstance,  $scope, Roles, Users) {

        Roles.findAll(function(roles){
            $scope.roles = roles;

            $scope.user = {
                name: "",
                lastName: "",
                email: "",
                password: "",
                role: roles[0]
            };
        });

        $scope.createUser = function () {
            Users.create($scope.user, function(){
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('ProductsModalInstanceCtrl', function ($http, $uibModalInstance,  $scope, Categories, Products, Sizes, Colors) {

        Categories.findAll(function(categories){
            $scope.categories = categories;
            $scope.product = {
                name: "",
                price: 0,
                description: "",
                quantity: 0,
                category: $scope.categories[0]
            };
        });

        Sizes.findAll(function(sizes){
            $scope.sizes = sizes;
            $scope.product.sizes = sizes;
        });
        Colors.findAll(function(colors){
            $scope.colors = colors;
            $scope.product.colors = colors;
        });

        $scope.createProduct = function () {
            Products.create($scope.product, function(product){
                var file = $scope.file;
                console.log(file)
                var fd = new FormData();
                fd.append('picture', file);
                console.log(fd);
                $http.post("/api/products/"+product.id+"/upload", fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined}
                    })
                    .then(function(){
                        $uibModalInstance.close();
                    });
            });

        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('RolesModalInstanceCtrl', function ($uibModalInstance,  $scope, Roles) {
        $scope.role = {
            name: ""
        };
        $scope.createRole = function () {
            Roles.create($scope.role, function(){
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('SizesModalInstanceCtrl', function ($uibModalInstance,  $scope, Sizes) {
        $scope.size = {
            name: ""
        };
        $scope.createSize = function () {
            Sizes.create($scope.size, function(){
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('CategoriesModalInstanceCtrl', function ($uibModalInstance,  $scope, Categories) {
        $scope.category = {
            name: ""
        };
        $scope.createCategory = function () {
            Categories.create($scope.category, function(){
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    })
    .controller('ColorsModalInstanceCtrl', function ($uibModalInstance,  $scope, Colors) {
        $scope.color = {
            name: "",
            hex: ""
        };
        $scope.createColor = function () {
            Colors.create($scope.color, function(){
                $uibModalInstance.close();
            });
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    });
